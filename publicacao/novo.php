<?php 

?>

<!doctype html>
<html lang="pt-br">
    <head>
        <title>PORTAL MKT</title>
        <link rel="icon" type="image/png" href="../static/icon/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../static/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../static/css/font-awesome.min.css" />    
        <link rel="stylesheet" href="../static/css/styles.css" />
        <script src="../static/js/jquery.min.js"></script>
        <script src="../static/js/index.js"></script>
        <script src="static/js/publicacao-novo.js"></script> 
    </head>
    <body>

    <header>
        <div id="menuBarNag" name="menuBarNag" ></div>
    </header>
    
    <section class="container py-5 my-5">
    <div class="row alert alert-primary text-center" role="alert">
        <h1 class="text-danger">Nova Publicação</h1>
    </div>
    <div class="row">

        <div class="col-6 py-5">
            <img src="../static/img/default-publicacao.png" id="output1" class="d-block w-100" alt="...">    
        </div>

        <div class="col-6">

            <div class="input-group mb-3">
                <span class="input-group-text w-25" for="publicacaoTitulo">Titulo</span>
                <input required type="text" id="publicacaoTitulo" placeholder="Titulo" name="publicacaoTitulo" class="form-control">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text w-25" for="publicacaoeDescricao">Descrição</span>
                <input required type="text" id="publicacaoeDescricao" placeholder="Descrição" name="publicacaoeDescricao" class="form-control">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text w-25" for="publicacaoLink">Link</span>
                <input required type="text" id="publicacaoLink" placeholder="Link externo" name="publicacaoLink" class="form-control">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text w-25">Tipo</span>
                <select required class="form-select" id="publicacaoTipo">
                    <option value="0" selected disabled>Selecione um tipo...</option>
                
                </select> 
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text w-25">Inicia</span>
                <input required type="date" id="publicacaoInicio" placeholder="Inicio da publicacao" name="publicacaoInicio" class="form-control">
                <span class="input-group-text w-25">Termina</span>
                <input required type="date" id="publicacaoTermino" placeholder="Termino da publicacao" name="publicacaoTermino" class="form-control">
            </div>
        
            <div class="input-group mb-3">
                <span class="input-group-text">Ordem</span>
                <input required type="number" id="publicacaoOrdem1" class="w-25">
                
                <input required type="file" accept="image/*" onchange="loadFile1(event)" class="form-control" id="imagem1">
            </div>

            <div class="alert alert-primary" role="alert">
                <p>Certifique-se de preencher todos os campos!</p>
                <div class="input-group mb-3">
                    <button id="botaoConfirmacao" type="submit" class="btn btn-primary mx-auto" data-bs-toggle="modal" data-bs-target="#staticBackdrop" data-bs-placement="top" title="DÊ 2 CLIQUES">
                        CONFIRMAR
                    </button>
                      
                    <div class="modal fade " id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div id="modalInicio" class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Confirmação do Novo Tabloide</h5>
                                </div>
                                <div id="modalMeio" class="modal-body">
                                    <div id="tabloideMensagem"></div>
                                        <h6>Confirme se todos os dados do novo tabloide estão correto:</h6>
                                            <div id="modalConfimacao"></div>
                                </div>

                                <div id="modalFim" class="modal-footer">
                                    <button id="botaoFechar" type="button" class="btn btn-light"> Fechar </button>
                                    <button id="botaoAguarde" type="button" class="btn btn-secondary" disabled> Aguarde . . . </button>
                                    <button id="botaoCancelar" type="button" class="btn btn-danger" data-bs-dismiss="modal"> Cancelar </button>
                                    <button id="botaoEnviar" type="button" class="btn btn-primary" onclick="EnviarTabloide(this)"> Enviar </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>      
    </div>
    </section>

        <script src="../static/js/bootstrap.min.js"></script>
        <script src="../static/js/popper.min.js"></script>      
    </body>

</html>