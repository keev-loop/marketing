<?php
session_start();
?>

<!doctype html>
<html lang="pt-br">
    <head>
        <title>PORTAL MKT</title>
        <link rel="icon" type="image/png" href="static/icon/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="static/css/bootstrap.min.css" />
        <link rel="stylesheet" href="static/css/font-awesome.min.css" /> 
        <link rel="stylesheet" href="static/css/styles.css" />
        <script src="static/js/jquery.min.js"></script>
        <script src="static/js/index.js"></script>
        <script src="static/js/conta.js"></script>
    </head>
    <body>

        <header>
            <div id="menuBarNag" name="menuBarNag" ></div>
        </header>
        
        <section class="container py-5 my-5">
           
                <div class="modal-dialog modal-lg box-shadow-nag">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-4">
                                    <figure class="figure">
                                        <img src="static/img/profile-foto.jpg" class="figure-img img-fluid rounded-circle" alt="...">
                                    </figure>
                                </div>
                                <div class="col-8">
                                    <form>
                                        <fieldset disabled>
                                            <legend id="usuarioNome">nome</legend>
                                            <div class="mb-3">
                                                <label for="usuarioSobrenome" class="form-label">Sobrenome</label>
                                                <input type="text" id="usuarioSobrenome" class="form-control" placeholder="">
                                            </div>
                                            <div class="mb-3">
                                                <label for="usuarioUsername" class="form-label">Usuario</label>
                                                <input type="text" id="usuarioUsername" class="form-control" placeholder="">
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <form>
                                        <fieldset disabled>
                                            <legend>Empresariais</legend>
                                            <div class="mb-3">
                                                <label for="usuarioLocal" class="form-label">Local</label>
                                                <input type="text" id="usuarioLocal" class="form-control" placeholder="">
                                            </div>
                                            <div class="mb-3">
                                                <label for="usuarioSetor" class="form-label">Setor</label>
                                                <input type="text" id="usuarioSetor" class="form-control" placeholder="">
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="col">
                                    <form>
                                        <fieldset disabled>
                                            <legend>Pessoais</legend>
                                            <div class="mb-3">
                                                <label for="usuarioNascimento" class="form-label">Nascimento</label>
                                                <input type="text" id="usuarioNascimento" class="form-control" placeholder="">
                                            </div>
                                            <div class="mb-3">
                                                <label for="usuarioTelefone" class="form-label">Telefone</label>
                                                <input type="text" id="usuarioTelefone" class="form-control" placeholder="">
                                            </div>
                                        </fieldset>
                                    </form>            
                                </div>   
                            </div>
                        </div>
                    </div>
                </div>
            
        </section>

        <script src="static/js/popper.min.js"></script>
        <script src="static/js/bootstrap.min.js"></script>      
    </body>

</html>