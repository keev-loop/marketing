<?php
session_start();
?>

<!doctype html>
<html lang="pt-br">
    <head>
        <title>PORTAL MKT</title>
        <link rel="icon" type="image/png" href="static/icon/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="static/css/bootstrap.min.css" />
        <link rel="stylesheet" href="static/css/font-awesome.min.css" /> 
        <link rel="stylesheet" href="static/css/styles.css" />
        <script src="static/js/jquery.min.js"></script>
        <script src="static/js/index.js"></script>
    </head>
    <body>

        <header>
            <div id="menuBarNag" name="menuBarNag" ></div>
        </header>
        
        <section class="container py-5 my-5">
            <h1>Bem vindo(a) ao Portal Marketing!</h1>
        </section>

        <script src="static/js/popper.min.js"></script>
        <script src="static/js/bootstrap.min.js"></script>      
    </body>

</html>