<?php
session_start();
?>

<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>PORTAL MKT</title> 
        <link rel="icon" type="image/png" href="static/icon/favicon.ico" />
        <link rel="stylesheet" href="static/css/bootstrap.min.css" />
        <link rel="stylesheet" href="static/css/styles.css" />
        <link rel="stylesheet" href="static/css/login.css" />
        <link rel="stylesheet" href="static/css/font-awesome.min.css" />   
        <script src="static/js/jquery.min.js"></script>
        <script src="static/js/login.js"></script> 
    </head>
    <body class="text-center">

        <main class="form-signin">

            <div id="loginMensagem"></div>

            <form action="" method="POST">
                <img class="mb-4 img-logo" src="static/img/logo-nagumo.png" alt="Logo Nagumo">
                <h1 class="h3 mb-3 fw-normal text-white">Portal Marketing</h1>

                <div class="form-floating">
                <h2 id="result"></h2>
                    <input type="email" class="form-control" id="login" name="login" placeholder="name@example.com" required>
                    <label for="login">Email</label>
                </div>

                <div class="form-floating mb-5">
                    <input type="password" class="form-control" id="senha" name="senha" placeholder="Senha" required>
                    <label for="senha">Senha</label>
                </div>
      
                <button class="w-100 btn btn-lg btn-primary" type="button" id="fazerLogin" name="fazerLogin"> Acessar <i class="fa fa-sign-in" aria-hidden="true"></i> </button>
                <p class="mt-5 mb-3 text-muted text-white">Nagumo Devs - © 2021–<?php echo date("Y"); ?></p>
            </form>
        </main>

        <script src="static/js/popper.min.js"></script>
        <script src="static/js/bootstrap.min.js"></script>      
    </body>

</html>