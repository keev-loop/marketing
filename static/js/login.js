const postDoLogin = "http://endpoint.meunagumo.com.br:8080/login";
//_> - FUNÇÃO PARA RETORNAR SE EMAIL É VALIDO
function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
//_> - FUNÇÃO QUE EXIBE NA PAGINA SE EMAIL É VALIDO OU NÃO
function validate() {
    const login = $("#login").val();

    if (validateEmail(login)) {
        $("#login").removeClass("is-valid");
        $("#login").removeClass("is-invalid");
        $("#login").addClass("is-valid");
    } else {
        $("#login").removeClass("is-valid");
        $("#login").removeClass("is-invalid");
        $("#login").addClass("is-invalid");
    }
    return false;
}

//_> - RETORNO HTML DE ERRO AO TENTAR FAZER LOGIN
loginComErro = `
    <div class="alert alert-danger" role="alert">
        Um erro aconteceu ao tentar acessar.<br>
        Tente novamente, ou contate a equipe de desenvolvimento.
    </div>
`;
loginIncorreto = `
    <div class="alert alert-warning" role="alert">
        Login ou Senha incorreto!<br>
        Tente novamente. <i class="fa fa-ban" aria-hidden="true"></i>
    </div>
`;
//_> - REQUISIÇÃO POST PARA FAZER LOGIN
function FazerLogin(login, senha) {
    let acesso = {
        "login": login,
        "password": senha
    };
    fetch(postDoLogin, {
        "method": "POST",
        "headers": {
            "Content-Type": "application/json"
        },
        "body": JSON.stringify(acesso)
    }).then(response => {
        return response.json();
    }).then(responseJson => {
        var diaLogin = new Date();
        if (responseJson.emailValid === true && responseJson.status === true && responseJson.token !== null) {
            localStorage.setItem('usuario', responseJson.nome);
            localStorage.setItem('status', responseJson.status);
            localStorage.setItem('token', responseJson.token);
            localStorage.setItem('emailValid', responseJson.emailValid);
            localStorage.setItem('emailUser', login);
            localStorage.setItem('logadoEm', diaLogin.getFullYear() + "" + (diaLogin.getMonth() + 1) + "" + diaLogin.getDate());
            window.location.href = "index.php";
        } else {
            localStorage.setItem('emailValid', false);
            localStorage.setItem('status', false);
            $("#loginMensagem").html(loginIncorreto);
        }
    }).catch(err => {
        $("#loginMensagem").html(loginComErro);
    });
}

//_> - CODIGO PRINCIPAL
$(document).ready(function() {
    $("#fazerLogin").on('click', function() {
        const login = $("#login").val();
        const senha = $("#senha").val();

        if (validateEmail(login) && senha.length > 3) {
            FazerLogin(login, senha);
        } else {
            alert("Login ou Senha inválidos!")
        }

    })
    $("#login").on("input change click", validate);
});