// ROUTES
let padrao = "/marketing";
let home = padrao + "/";
let login = padrao + "/login.php";
let conta = padrao + "/conta.php";

let tabloideInicio = padrao + "/tabloide";
let tabloideNovo = padrao + "/tabloide/novo.php";
let tabloideConsulta = padrao + "/tabloide/consulta.php";
let tabloideEdita = padrao + "/tabloide/edita.php";
let tabloideVer = "http://tabloide.nagumo.com.br/v3/";

let publicacaoInicio = padrao + "/publicacao";
let publicacaoNovo = padrao + "/publicacao/novo.php";
let publicacaoConsulta = padrao + "/publicacao/consulta.php";
let publicacaoEdita = padrao + "/publicacao/edita.php";

let cardapioInicio = padrao + "/cardapio";
let cardapioNovo = padrao + "/cardapio/novo.php";
let cardapioConsulta = padrao + "/cardapio/consulta.php";
let cardapioEdita = padrao + "/cardapio/edita.php";


// MAIN MENU
navBarNag = `
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary box-shadow-nag nav-menu-nag">
    <div class="container-fluid ">
        
        <div class="">
            <button class="btn btn-outline-primary" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasWithBackdrop" aria-controls="offcanvasWithBackdrop">
                <a class="navbar-brand">Menu</a>
            </button>

            <div class="offcanvas offcanvas-start" tabindex="-1" id="offcanvasWithBackdrop" aria-labelledby="offcanvasWithBackdropLabel">
                <div class="offcanvas-header">
                    <h5 class="offcanvas-title" id="offcanvasWithBackdropLabel">Menu</h5>
                    <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                </div>
                <div class="offcanvas-body">             
                    <ul class="div-opcao">
                        <div class="d-grid gap-2">
                            <a href="${tabloideInicio}" type="button" class="btn button-menu-nag btn-lg">Tabloides</a>
                        </div>
                        <a href="${tabloideNovo}" class="list-group-item btn-outline-primary">Novo Tabloide</a>
                        <a href="${tabloideConsulta}" class="list-group-item btn-outline-primary" >Consultar Tabloide</a>
                        <a href="${tabloideEdita}" class="list-group-item btn-outline-primary" >Editar Tabloide</a>
                        <a href="${tabloideVer}" class="list-group-item btn-outline-primary" >Ver Tabloides</a>
                    </ul>
                
                    <ul class="div-opcao">
                    <div class="d-grid gap-2">
                        <a href="${publicacaoInicio}" type="button" class="btn button-menu-nag btn-lg" >Publicações</a>
                    </div>
                    <a href="${publicacaoNovo}" class="list-group-item btn-outline-primary">Nova Publicação</a>
                    <a href="${publicacaoConsulta}" class="list-group-item btn-outline-primary">Consultar Publicação</a>
                    <a href="${publicacaoEdita}" class="list-group-item btn-outline-primary">Editar Publicação</a>
                    </ul>
                
                    <ul class="div-opcao">
                    <div class="d-grid gap-2">
                        <a href="${cardapioInicio}" type="button" class="btn button-menu-nag btn-lg" >Cardapios</a>
                    </div>
                    <a href="${cardapioNovo}" class="list-group-item btn-outline-primary">Novo Cardapio</a>
                    <a href="${cardapioConsulta}" class="list-group-item btn-outline-primary">Consultar Cardapio</a>
                    <a href="${cardapioEdita}" class="list-group-item btn-outline-primary">Editar Cardapio</a>
                    </ul>
                
                    <ul class="div-opcao">
                    <div class="d-grid gap-2">
                        <button type="button" class="btn button-menu-nag btn-lg" >Ofertas</button>
                    </div>
                    <a href="#" class="list-group-item btn-outline-primary">Nova Oferta</a>
                    <a href="#" class="list-group-item btn-outline-primary">Editar Oferta</a>
                    <a href="#" class="list-group-item btn-outline-primary">Inativar Oferta</a>
                    </ul>
                    
                </div>
            </div>
            
        </div>
     
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav me-auto mb-2 mb-md-0">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="${home}">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <form class="d-flex">
                    <div class="dropdown">
                    <button class="btn btn-outline-primary dropdown-toggle text-white" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                        Conta
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><a class="dropdown-item" href="${conta}">Minha conta</a></li>
                        <li><button class="dropdown-item" type="button" onclick="fazerLogout();">Sair</button></li>
                    </ul>
                    </div>

                    <input class="form-control me-2" type="search" placeholder="Buscar . . ." aria-label="Search">
                    <button class="btn btn-outline-light" type="submit">Buscar</button>
                </form>
            </div>      
    </div>
</nav>
`;


// WORKING
$(document).ready(function() {
    $("#emObras").html(`
    <div class="row mx-auto">
        <figure class="figure text-center">
            <figcaption class="figure-caption"><h4>Estamos em construção.</h4></figcaption>
            <img src="../static/img/obra.png" class="figure-img img-fluid rounded" alt="Trabalhando...">
            <figcaption class="figure-caption"><h4>Aguarde...</h4</figcaption>
        </figure>
    </div>`);
});



// LOGOUT
function fazerLogout() {
    localStorage.clear();
    location.replace(login);
}


// IF AUTHENTICATED
if (!localStorage.getItem('status') || !localStorage.getItem('emailValid') || (localStorage.getItem('token')).length < 172) {
    fazerLogout();
} else {
    $(document).ready(function() {
        $("#menuBarNag").html(navBarNag);
    });
}