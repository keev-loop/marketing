// DEFINE VARIABLES
let USUARIO = "http://endpoint.meunagumo.com.br:8080/admin/participante?cpf=";
let TOKEN = localStorage.getItem('token');
let LOGIN = localStorage.getItem('emailUser');


// CREATE HTML WITH RESPONSE
function criaPerfilHtml(json) {
    //console.log(json);
    $("#usuarioNome").html("<p>" + json.nome + "<p>");
    $("#usuarioSobrenome").val(json.sobrenome);
    $("#usuarioUsername").val(json.username);
    $("#usuarioLocal").val("Matriz");
    $("#usuarioSetor").val(json.observacao);
    $("#usuarioNascimento").val(json.dataNascimento);
    $("#usuarioTelefone").val("(" + json.ddd + ") " + (json.telefone).substring(0, 5) + "-" + (json.telefone).substring(5, 9));
}


// REQUEST USUARIO
function ExibirCadastroUsuario() {
    fetch(USUARIO + LOGIN, {
            "method": "GET",
            "headers": { "Authorization": TOKEN }
        })
        .then(response => response.json())
        .then(responseJson => criaPerfilHtml(responseJson))
        .catch(err => console.log(err));
}


// MAIN
ExibirCadastroUsuario();