const getCategoriasAPI = 'http://35.168.166.249:9090/api/v1/categoria';
const postTabloideAPI = 'http://35.168.166.249:9090/api/v1/tabloide/uploadx';
/*const getCategoriasAPI = 'http://172.16.18.1:9090/api/v1/categoria';
const postTabloideAPI = 'http://172.16.18.1:9090/api/v1/tabloide/uploadx';*/

let titulo;
let descricao;
let tipo;
let inicio;
let termino;
let lojinhas;
let ordem;
var listaLojas = [];


// - Adiciona/Remove as LOJAS em um Array e exibe como uma unica String
/* function addLoja(loja) {
    listaLojas.push(loja);
}

function removeLoja(loja) {
    const index = listaLojas.indexOf(loja);
    listaLojas.splice(index, 1);
} */

function chamaFuncao(loja) {
    if (loja === 0) {
        listaLojas = [];
    } else if (listaLojas.indexOf(loja) === -1) {
        listaLojas.push(loja);
    } else {
        const index = listaLojas.indexOf(loja);
        listaLojas.splice(index, 1);
    }
    lojinhas = listaLojas.toString();
}
// - FIM


// - FUNÇÃO QUE FORMATA DATA
function convertDate(dia) {
    return dia.substring(0, 4) + "" + dia.substring(5, 7) + "" + dia.substring(8, 10);
}
// - FIM


// - MENSAGEM DO CADASTRO DE TABLOIDE
let mensagemErro = `
    <div class="alert alert-danger" role="alert">
        Parece que houve um erro ao tentar executar a solicitação.<br>
        Entre em contato com equipe de desenvolvimento.<br>
        <i class="fa fa-frown-o" aria-hidden="true"></i>
    </div>
    `;
let mensagemAguarde = `
    <div class="alert alert-warning" role="alert">
        O novo tabloide está sendo cadastrado.<br>
        Aguarde pela finalização.<br>
        Aguarde . . . 
        <i class="fa fa-cog fa-spin fa-3x fa-fw" aria-hidden="true"></i>
        <span class="sr-only">Salvando...</span>
    </div>
    `;
let mensagemSucesso = `
    <div class="alert alert-success" role="alert">
        Tabloide cadastrado com sucesso!<br>
        Obrigado!<br>
        <i class="fa fa-check-square-o" aria-hidden="true"></i>
    </div>
    `;
// - FIM


// - RENOMEA O NOME FO ARQUIVO
function renomearFile(arquivo, novoNome) {
    if (arquivo === undefined) {
        return undefined;
    } else {
        var blob = arquivo.slice(0, arquivo.size, 'image/png');
        newFile = new File([blob], novoNome + '.png', { type: 'image/png' });
        return newFile;
    }
}
// - FIM


// - CONFIRMA O TABLOIDE ANTES DO ENVIO
function ConfirmarTabloide() {
    let geraHtmlModal = `
                    <dl class="row container">
                        <dd class="col-sm-4">Titulo:</dd><dt class="col-sm-6"> ${titulo !== undefined ? titulo : "CAMPO VAZIO"}</dt>
                        <dd class="col-sm-4">Descrição:</dd><dt class="col-sm-6"> ${descricao !== undefined ? descricao : "CAMPO VAZIO"}</dt>
                        <dd class="col-sm-4">Tipo:</dd><dt class="col-sm-6"> ${tipo !== undefined ? tipo : "CAMPO VAZIO"}</dt>
                        <dd class="col-sm-4">Inicio:</dd><dt class="col-sm-6"> ${inicio !== undefined ? inicio : "CAMPO VAZIO"}</dt>
                        <dd class="col-sm-4">Termino:</dd><dt class="col-sm-6"> ${termino !== undefined ? termino : "CAMPO VAZIO"}</dt>
                        <dd class="col-sm-4">Lojas:</dd><dt class="col-sm-6 max-size-ngm"> ${lojinhas !== undefined ? lojinhas : "CAMPO VAZIO"}</dt>
                    </dl>
                        `;
    return $("#modalConfimacao").html(geraHtmlModal);
}
// - FIM


// - ENVIAR REQUISAO PARA INCLUIR TABLOIDE
function aguardeEnviando() {
    $("#tabloideMensagem").html(mensagemAguarde);
    $("#botaoEnviar").hide();
    $("#botaoCancelar").hide();
    $("#botaoAguarde").show();
    $("#modalInicio").removeClass("alert-primary");
    $("#modalFim").removeClass("alert-primary");
    $("#modalInicio").addClass("alert-warning");
    $("#modalFim").addClass("alert-warning");
}

function envioSucesso() {
    $("#botaoAguarde").hide();
    $("#tabloideMensagem").html(mensagemSucesso);
    $("#modalInicio").removeClass("alert-warning");
    $("#modalFim").removeClass("alert-warning");
    $("#modalInicio").addClass("alert-success");
    $("#modalFim").addClass("alert-success");
    $("#botaoFechar").show();
}

function envioFalha() {
    $("#botaoAguarde").hide();
    $("#tabloideMensagem").html(mensagemErro);
    $("#modalInicio").removeClass("alert-warning");
    $("#modalFim").removeClass("alert-warning");
    $("#modalInicio").addClass("alert-danger");
    $("#modalFim").addClass("alert-danger");
}

function envioErro() {
    $("#botaoAguarde").hide();
    $("#tabloideMensagem").html(mensagemErro);
    $("#modalInicio").removeClass("alert-warning");
    $("#modalFim").removeClass("alert-warning");
    $("#modalInicio").addClass("alert-dark");
    $("#modalFim").addClass("alert-dark");
}

function EnviarTabloide(e) {
    if (inicio > 20210000 && termino > 20210000 && termino >= inicio && (titulo).length > 5 && validateTexto(titulo) &&
        validateTexto(descricao) && (descricao).length > 5 && (tipo).length > 5 && ordem > 0 && (lojinhas).length > 0) {

        aguardeEnviando();
        let arquivo1 = renomearFile(document.getElementById("imagem1").files[0], document.getElementById('tabloideOrdem1').value);
        let arquivo2 = renomearFile(document.getElementById("imagem2").files[0], document.getElementById('tabloideOrdem2').value);
        let arquivo3 = renomearFile(document.getElementById("imagem3").files[0], document.getElementById('tabloideOrdem3').value);
        let arquivo4 = renomearFile(document.getElementById("imagem4").files[0], document.getElementById('tabloideOrdem4').value);
        //let files = renomearFile(document.getElementById("imagem1").files[0], document.getElementById('tabloideOrdem1').value);
        let tabloide = {
            tabloideAtivo: true,
            tabloideTitulo: document.getElementById("tabloideTitulo").value,
            tabloideDescricao: document.getElementById("tabloideDescricao").value,
            tabloideLoja: lojinhas,
            tabloideTipo: document.getElementById("tabloideTipo").value,
            tabloideInicio: convertDate(document.getElementById('tabloideInicio').value),
            tabloideTermino: convertDate(document.getElementById('tabloideTermino').value)
        };

        let formData = new FormData();
        arquivo1 !== undefined ? formData.append("files", arquivo1) : console.log("img1 vazio");
        arquivo2 !== undefined ? formData.append("files", arquivo2) : console.log("img2 vazio");
        arquivo3 !== undefined ? formData.append("files", arquivo3) : console.log("img3 vazio");
        arquivo4 !== undefined ? formData.append("files", arquivo4) : console.log("img4 vazio");
        //formData.append("files", files);
        formData.append("tabloide", JSON.stringify(tabloide));

        fetch(postTabloideAPI, { method: "POST", body: formData })
            .then((response) => {
                if (response.status >= 200 && response.status < 300) {
                    envioSucesso();
                } else if (response.status >= 300 && response.status < 500) {
                    envioFalha();
                } else {
                    envioErro();
                }
            })
            .catch(error => {
                envioErro();
            });
    } else {
        alert("Existem campos invalidos!")
    }

}
// - FIM


// - AGUARDA MUDANÇA, QUANDO ALTERADO PEGA O VALOR DO CAMPO E PREENCHE NO HTML
/* var loadFile1 = function(event) {
    var output = document.getElementById('output1');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
    }
};
var loadFile2 = function(event) {
    var output = document.getElementById('output2');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
    }
};
var loadFile3 = function(event) {
    var output = document.getElementById('output3');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
    }
};
var loadFile4 = function(event) {
    var output = document.getElementById('output4');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function() {
        URL.revokeObjectURL(output.src) // free memory
    }
}; */

function loadFile(saida) {
    return function(event) {
        var output = document.getElementById(saida);
        output.src = URL.createObjectURL(event.target.files[0]);
        output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
        }
    };
}
// - FIM


//> - VALIDA SE TEXTO É VALIDO
function validateTexto(texto) {
    const re = /^(([a-zA-Z0-9]{6,30}))$/;
    return re.test(texto);
}
//_> - FUNÇÃO QUE EXIBE NA PAGINA SE TEXTOS SÃO VALIDOS
function validateTxt1() {
    const tabloideTitulo = $("#tabloideTitulo").val();
    if (validateTexto(tabloideTitulo)) {
        $("#tabloideTitulo").removeClass("is-valid");
        $("#tabloideTitulo").removeClass("is-invalid");
        $("#tabloideTitulo").addClass("is-valid");
    } else {
        $("#tabloideTitulo").removeClass("is-valid");
        $("#tabloideTitulo").removeClass("is-invalid");
        $("#tabloideTitulo").addClass("is-invalid");
    }
    return false;
}

function validateTxt2() {
    const tabloideDescricao = $("#tabloideDescricao").val();
    if (validateTexto(tabloideDescricao)) {
        $("#tabloideDescricao").removeClass("is-valid");
        $("#tabloideDescricao").removeClass("is-invalid");
        $("#tabloideDescricao").addClass("is-valid");
    } else {
        $("#tabloideDescricao").removeClass("is-valid");
        $("#tabloideDescricao").removeClass("is-invalid");
        $("#tabloideDescricao").addClass("is-invalid");
    }
    return false;
}
// - FIM


// - CODIGO PRINCIPAL
$(document).ready(function() {

    $("#botaoFechar").hide();
    $("#botaoAguarde").hide();
    $("#modalInicio").addClass("alert alert-primary");
    $("#modalFim").addClass("alert alert-primary");
    $("#tabloideTitulo").on("input change click", validateTxt1);
    $("#tabloideDescricao").on("input change click", validateTxt2);

    $("#imagem1").on("input change", loadFile('output1'));
    $("#imagem2").on("input change", loadFile('output2'));
    $("#imagem3").on("input change", loadFile('output3'));
    $("#imagem4").on("input change", loadFile('output4'));

    $("#tabloideTitulo").on('input click change', function() {
        titulo = document.getElementById("tabloideTitulo").value;
        ConfirmarTabloide();
    })
    $("#tabloideDescricao").on('input click change', function() {
        descricao = document.getElementById("tabloideDescricao").value;
        ConfirmarTabloide();
    })
    $("#tabloideTipo").on('input click change', function() {
        tipo = document.getElementById("tabloideTipo").value;
        ConfirmarTabloide();
    })
    $("#tabloideInicio").on('input click change', function() {
        inicio = convertDate(document.getElementById('tabloideInicio').value);
        ConfirmarTabloide();
    })
    $("#tabloideTermino").on('input click change', function() {
        termino = convertDate(document.getElementById('tabloideTermino').value);
        ConfirmarTabloide();
    })
    $("#tabloideOrdem1").on('input click change', function() {
        ordem = document.getElementById('tabloideOrdem1').value;
        ConfirmarTabloide();
    })
    $("#botaoConfirmacao").click(function() {
        //lojinhas = listaLojas.toString();
        ConfirmarTabloide();
    })
    $("#botaoFechar").click(function() {
        location.reload(true);
    })

    $("#setBtnSp").click(function() {
        chamaFuncao(0);
        var lojas = ["btncheck01", "btncheck02", "btncheck03", "btncheck04", "btncheck06", "btncheck07", "btncheck08", "btncheck09", "btncheck10", "btncheck11", "btncheck12", "btncheck13", "btncheck14", "btncheck15", "btncheck16", "btncheck17", "btncheck18", "btncheck19", "btncheck20", "btncheck21", "btncheck22", "btncheck23", "btncheck24", "btncheck25", "btncheck26", "btncheck27", "btncheck28", "btncheck29", "btncheck30", "btncheck32", "btncheck33", "btncheck34", "btncheck35", "btncheck37", "btncheck38", "btncheck39", "btncheck40", "btncheck42", "btncheck43", "btncheck44", "btncheck45", "btncheck47", "btncheck48", "btncheck49", "btncheck50", "btncheck51", "btncheck52"];
        $('.btn-check[type="checkbox"]').map(function() {
            lojas.includes($(this).val()) ? $(this).prop('checked', true) : $(this).prop('checked', false)
        });
        for (let count = 1; count < 54; count++) {
            if (count !== 5 && count !== 36 && count !== 53 && count !== 31 && count !== 41) {
                chamaFuncao(count);
            }
        }
    })
    $("#setBtnMix").click(function() {
        chamaFuncao(0);
        var lojas = ["btncheck31", "btncheck41"];
        $('.btn-check[type="checkbox"]').map(function() {
            lojas.includes($(this).val()) ? $(this).prop('checked', true) : $(this).prop('checked', false)
        });
        chamaFuncao(31);
        chamaFuncao(41);
    });
    $("#setBtnRj").click(function() {
        chamaFuncao(0);
        var lojas = ["btncheck36", "btncheck53"];
        $('.btn-check[type="checkbox"]').map(function() {
            lojas.includes($(this).val()) ? $(this).prop('checked', true) : $(this).prop('checked', false)
        });
        chamaFuncao(36);
        chamaFuncao(51);
    });
    $("#setBtnTodas").click(function() {
        chamaFuncao(0);
        var lojas = [];
        for (let count = 1; count < 54; count++) {
            if (count === 5) {
                //console.log("Loja 5 Pula")
            } else if (count < 10) {
                lojas.push("btncheck0" + count.toString());
                chamaFuncao(count);
            } else {
                lojas.push("btncheck" + count.toString());
                chamaFuncao(count);
            }
        }
        $('.btn-check[type="checkbox"]').map(function() {
            lojas.includes($(this).val()) ? $(this).prop('checked', true) : $(this).prop('checked', false)
        });
    });
    $("#setBtnLimpa").click(function() {
        chamaFuncao(0);
        var lojas = [];
        $('.btn-check[type="checkbox"]').map(function() {
            lojas.includes($(this).val()) ? $(this).prop('checked', true) : $(this).prop('checked', false)
        });
    });


});
// - FIM