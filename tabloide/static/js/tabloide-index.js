const getTodosTabloidesHoje = 'http://35.168.166.249:9090/api/v1/tabloide/hoje';
const getTabloidePeloId = 'http://35.168.166.249:9090/api/v1/tabloide/';


// - FUNÇÃO FORMATAR DATA
function convertDate(dia) {
    return (dia.toString()).substring(6, 9) + "-" + (dia.toString()).substring(4, 6) + "-" + (dia.toString()).substring(0, 4);
}
// - FIM

// - GERA MODAL DO TABLOIDE
function modalTabloide(responseJson) {
    return `        
    <div class="modal-body">
    <div class="accordion" id="accordionExample">
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingOne">
                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Detalhes
                </button>
            </h2>
            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <dl class="row container">
                        <dd class="col-sm-6">Status:</dd><dt class="col-sm-6"> ${responseJson.tabloideAtivo}</dt>
                        <dd class="col-sm-6">Id:</dd><dt class="col-sm-6"> ${responseJson.tabloideId}</dt>
                        <dd class="col-sm-6">Serie:</dd><dt class="col-sm-6"> ${responseJson.tabloideSerie}</dt>
                        <dd class="col-sm-6">Titulo:</dd><dt class="col-sm-6"> ${responseJson.tabloideTitulo}</dt>
                        <dd class="col-sm-6">Descrição:</dd><dt class="col-sm-6"> ${responseJson.tabloideDescricao}</dt>
                        <dd class="col-sm-6">Tipo:</dd><dt class="col-sm-6"> ${responseJson.tabloideTipo}</dt>
                        <dd class="col-sm-6">Inclusão:</dd><dt class="col-sm-6"> ${convertDate(responseJson.tabloideInclusao)}</dt>
                        <dd class="col-sm-6">Inicio:</dd><dt class="col-sm-6"> ${convertDate(responseJson.tabloideInicio)}</dt>
                        <dd class="col-sm-6">Termino:</dd><dt class="col-sm-6"> ${convertDate(responseJson.tabloideTermino)}</dt>
                        <dd class="col-sm-6">Loja:</dd><dt class="col-sm-6"> ${responseJson.tabloideLoja}</dt>
                    </dl>
                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header" id="headingTwo">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    Imagem
                </button>
            </h2>
            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                <div class="accordion-body">
                    <img src="${responseJson.tabloideImagem}" class="d-block w-100" alt="Tabloide">
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fechar</button>
        <a type="button" class="btn btn-primary" href="edita.php?tabloideId=${responseJson.tabloideId}">Editar</a>
    </div>
    `;
}
// - FIM

// - REQUEST DO TABLOIDE E CHAMA MODAL
function ExibirTabloide(tabloideId) {
    fetch(getTabloidePeloId + tabloideId, { "method": "GET" })
        .then(response => {
            return response.json();
        })
        .then(responseJson => {
            //let geraHtmlModal = ;
            //$("#tabloideDetalhes").html(geraHtmlModal);
            $("#tabloideDetalhes").html(modalTabloide(responseJson));
        })
        .catch(err => {
            //$("#tabelaTabloides").html(carrosselIntervalo);
            console.log(err);
        });
}
// - FIM

// - GERA TABELA COM TABLOIDES VIGENTES HOJE
function GeraTabela(responseJson) {
    var saida = "";
    for (var i = 0; i < responseJson.length; i++) {
        saida = saida + `        
            <tr type="button" class="btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" onclick="ExibirTabloide(${responseJson[i].tabloideId})">
                <td>${responseJson[i].tabloideId}</td>
                <td>${responseJson[i].tabloideTitulo}</td>
                <td>${responseJson[i].tabloideTipo}</td>
                <td>${responseJson[i].tabloideLoja}</td>
                <td>${convertDate(responseJson[i].tabloideInclusao)}</td>
                <td>${convertDate(responseJson[i].tabloideInicio)}</td>
                <td>${convertDate(responseJson[i].tabloideTermino)}</td>
            </tr>`;
    }
    return saida;
}
// - FIM

// - CHAMA, RECEBE E GERA TABLOIDES
function GetTodosTabloides() {
    fetch(getTodosTabloidesHoje, { "method": "GET" })
        .then(response => {
            return response.json();
        })
        .then(responseJson => {
            if (responseJson.length === 0) {
                $("#tabelaTabloides").html(carrosselIntervalo);
            } else {
                $("#tabelaTabloides").html(GeraTabela(responseJson));
            }
        })
        .catch(err => {
            //$("#tabelaTabloides").html(carrosselIntervalo);
            console.log(err);
        });
}
// - FIM

// - CODIGO PRINCIPAL
$(document).ready(function() {
    GetTodosTabloides();
});
// - FIM