<?php 
function categorias() {
    $tipo = "";
    $response = file_get_contents('http://35.168.166.249:9090/api/v1/categoria');
    $response_data = json_decode($response);
    foreach ($response_data as &$value) {
        $tipo = $tipo . "<option value=".$value->categoriaNome.">".$value->categoriaNome."</option>";
    }
    return $tipo;
}
function lojas() {
    $valor = '';
    for ($x = 1; $x <= 53; $x++) {
        if($x == 5) { $x += 1; }
        if($x < 10) {$y = '0'; } else { $y = ''; }
        $valor = $valor . ' 
                            <div class="col w-100 p-0 m-0">
                                <input type="checkbox" class="btn-check " id="btncheck' . $y.$x . '" value="btncheck' . $y.$x . '" name="lojas" autocomplete="off"  onclick="chamaFuncao('.$x.')">
                                <label class="btn btn-outline-primary botao-lojas-ngm" for="btncheck' . $y.$x . '">' . $y.$x . '</label>
                            </div>
                            ';
      }
    return $valor;
}
?>

<!doctype html>
<html lang="pt-br">
    <head>
        <title>PORTAL MKT</title>
        <link rel="icon" type="image/png" href="../static/icon/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../static/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../static/css/font-awesome.min.css" />    
        <link rel="stylesheet" href="../static/css/styles.css" />
        <script src="../static/js/jquery.min.js"></script>
        <script src="../static/js/index.js"></script>
        <script src="static/js/tabloide-novo.js"></script> 
    </head>
    <body>

    <header>
        <div id="menuBarNag" name="menuBarNag" ></div>
    </header>
    
    <section class="container py-5 my-5">
    <div class="row alert alert-primary text-center" role="alert">
        <h1 class="text-danger">Novo Tabloide</h1>
    </div>
    <div class="row">
        <div class="col-6">
            <div id="carouselExampleControls" class="carousel slide" data-bs-interval="false" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    <button type="button" data-bs-target="#carouselExampleControls" data-bs-slide-to="3" aria-label="Slide 4"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="https://d39ytr32u863b6.cloudfront.net/assets/tabloide/intervalo.jpg" id="output1" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://d39ytr32u863b6.cloudfront.net/assets/tabloide/intervalo.jpg" id="output2" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://d39ytr32u863b6.cloudfront.net/assets/tabloide/intervalo.jpg" id="output3" class="d-block w-100" alt="...">
                    </div>
                    <div class="carousel-item">
                        <img src="https://d39ytr32u863b6.cloudfront.net/assets/tabloide/intervalo.jpg" id="output4" class="d-block w-100" alt="...">
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Voltar</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Avançar</span>
                </button>
            </div>
        </div>

        <div class="col-6">

            <div class="input-group mb-3">
                <span class="input-group-text w-25" for="tabloideTitulo">Titulo</span>
                <input required type="text" id="tabloideTitulo" placeholder="Titulo" name="tabloideTitulo" class="form-control">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text w-25" for="tabloideDescricao">Descrição</span>
                <input required type="text" id="tabloideDescricao" placeholder="Descricao" name="tabloideDescricao" class="form-control">
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text w-25">Tipo</span>
                <select required class="form-select" id="tabloideTipo">
                    <option value="0" selected disabled>Selecione um tipo...</option>
                '<?php echo categorias() ?>'
                </select> 
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text w-25">Inicia</span>
                <input required type="date" id="tabloideInicio" placeholder="Inicio" name="tabloideInicio" class="form-control">
                <span class="input-group-text w-25">Termina</span>
                <input required type="date" id="tabloideTermino" placeholder="Termino" name="tabloideTermino" class="form-control">
            </div>
        
            

            <div required class="btn-group row m-2 mb-3 div-lojas" role="group" aria-label="Basic checkbox toggle button group">
                <span class="input-group-text d-flex justify-content-around">
                    <label class="">Lojas</label>
                    <button type="button" class="btn btn-outline-primary" id="setBtnSp">SP</button>     
                    <button type="button" class="btn btn-outline-primary" id="setBtnMix">MIX</button>
                    <button type="button" class="btn btn-outline-primary" id="setBtnRj">RJ</button>
                    <button type="button" class="btn btn-outline-primary" id="setBtnTodas">TODAS</button>
                    <button type="button" class="btn btn-outline-primary" id="setBtnLimpa">LIMPA</button>
                </span>
                <?php echo lojas(); ?>
            </div>

            <div class="input-group mb-3">
                <span class="input-group-text">Ordem</span>
                <input type="number" id="tabloideOrdem1" class="w-25" required>
                
                <input type="file" accept="image/*" class="form-control" id="imagem1" required>
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text">Ordem</span>
                <input type="number" id="tabloideOrdem2" class="w-25">

                <input type="file" accept="image/*" class="form-control" id="imagem2">
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text">Ordem</span>
                <input type="number" id="tabloideOrdem3" class="w-25">

                <input type="file" accept="image/*" class="form-control" id="imagem3">
            </div>
            <div class="input-group mb-3">
                <span class="input-group-text">Ordem</span>
                <input type="number" id="tabloideOrdem4" class="w-25">

                <input type="file" accept="image/*" class="form-control" id="imagem4">
            </div>

            <div class="alert alert-primary" role="alert">
                <p>Certifique-se de preencher todos os campos!</p>
                <div class="input-group mb-3">
                    <button id="botaoConfirmacao" type="submit" class="btn btn-primary mx-auto" data-bs-toggle="modal" data-bs-target="#staticBackdrop" data-bs-placement="top" title="DÊ 2 CLIQUES">
                        CONFIRMAR
                    </button>
                      
                    <div class="modal fade " id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div id="modalInicio" class="modal-header">
                                    <h5 class="modal-title" id="staticBackdropLabel">Confirmação do Novo Tabloide</h5>
                                </div>
                                <div id="modalMeio" class="modal-body">
                                    <div id="tabloideMensagem"></div>
                                        <h6>Confirme se todos os dados do novo tabloide estão correto:</h6>
                                            <div id="modalConfimacao"></div>
                                </div>

                                <div id="modalFim" class="modal-footer">
                                    <button id="botaoFechar" type="button" class="btn btn-light"> Fechar </button>
                                    <button id="botaoAguarde" type="button" class="btn btn-secondary" disabled> Aguarde . . . </button>
                                    <button id="botaoCancelar" type="button" class="btn btn-danger" data-bs-dismiss="modal"> Cancelar </button>
                                    <button id="botaoEnviar" type="button" class="btn btn-primary" onclick="EnviarTabloide(this)"> Enviar </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>      
    </div>
    </section>

        <script src="../static/js/bootstrap.min.js"></script>
        <script src="../static/js/popper.min.js"></script>      
    </body>

</html>