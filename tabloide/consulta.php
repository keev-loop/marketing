<?php
session_start();

function categorias() {
    $tipo = "";
    $response = file_get_contents('http://35.168.166.249:9090/api/v1/categoria');
    $response_data = json_decode($response);
    foreach ($response_data as &$value) {
        $tipo = $tipo . "<option value=".$value->categoriaNome.">".$value->categoriaNome."</option>";
    }
    return $tipo;
}
function lojas() {
    $valor = '';
    for ($x = 1; $x <= 52; $x++) {
        if($x == 5) { $x += 1; }
        if($x < 10) {$y = '0'; } else { $y = ''; }
        $valor = $valor . "<option value=".$x."> Loja ".$y.$x."</option>";
      }
    return $valor;
}
?>

<!doctype html>
<html lang="pt-br">
    <head>
        <title>PORTAL MKT</title>
        <link rel="icon" type="image/png" href="../static/icon/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../static/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../static/css/font-awesome.min.css" />    
        <link rel="stylesheet" href="../static/css/styles.css" />
        <script src="../static/js/jquery.min.js"></script>
        <script src="../static/js/index.js"></script>
        <script src="static/js/tabloide-consulta.js"></script> 
    </head>
    <body>
        <header>
            <div id="menuBarNag" name="menuBarNag" ></div>
        </header>

        <section class="container py-5 my-5">
            <div class="row alert alert-primary text-center" role="alert">
                <h1 class="text-danger">Consultar Publicação</h1>
            </div>
            <div class="row">
                <div class="mb-3 col-sm-3">
                    <span class="input-group-text">Tipo</span>
                    <select required class="form-select" id="tabloideTipo">
                        <option value="0" selected disabled>Selecione um tipo...</option>
                        '<?php echo categorias() ?>'
                    </select> 
                </div>
                <div class=" mb-3 col-sm-3">
                    <span class="input-group-text">Loja</span>
                    <select required class="form-select" id="tabloideTipo">
                        <option value="0" selected disabled>Selecione uma loja...</option>
                        '<?php echo lojas() ?>'
                    </select> 
                </div>
                <div class="col-sm-3">
                    <span class="input-group-text">Inicia</span>
                    <input required type="date" id="tabloideInicio" placeholder="tabloideInicio" name="tabloideInicio" class="form-control">
                </div>
                <div class="col-sm-3">
                    <span class="input-group-text">Termina</span>
                    <input required type="date" id="tabloideTermino" placeholder="tabloideTermino" name="tabloideTermino" class="form-control">
                </div>

            </div>
        </section>

        <script src="../static/js/bootstrap.min.js"></script>
        <script src="../static/js/popper.min.js"></script>      
    </body>
</html>