<?php

?>

<!doctype html>
<html lang="pt-br">
    <head>
        <title>PORTAL MKT</title>
        <link rel="icon" type="image/png" href="../static/icon/favicon.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../static/css/bootstrap.min.css" />
        <link rel="stylesheet" href="../static/css/font-awesome.min.css" />    
        <link rel="stylesheet" href="../static/css/styles.css" />
        <script src="../static/js/jquery.min.js"></script>
        <script src="../static/js/index.js"></script>
        <script src="static/js/tabloide-index.js"></script> 
    </head>
    <body>
        <header>
            <div id="menuBarNag" name="menuBarNag" ></div>
        </header>

        <section class="container py-5 my-5">
            <div class="row alert alert-primary text-center" role="alert">
                <h1 class="text-danger">Tabloides Hoje</h1>
            </div>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Titulo</th>
                        <th scope="col">Tipo</th>
                        <th scope="col">Loja</th>
                        <th scope="col">Inclusão</th>
                        <th scope="col">Inicio</th>
                        <th scope="col">Termino</th>
                    </tr>
                </thead>

                <tbody id="tabelaTabloides"></tbody>
            </table>
        </section>

        <section>
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg ">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Tabloide</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                       
                        <div id="tabloideDetalhes"></div>  
                    </div>
                </div>
            </div>
        </section>

        <script src="../static/js/bootstrap.min.js"></script>
        <script src="../static/js/popper.min.js"></script>      
    </body>

</html>